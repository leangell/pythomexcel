import pprint

import openpyxl
import re
def is_email(email):
    regex = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
    return re.match(regex, email) is not None if email is not None else False

def intvalide(number):
        return isinstance(number,int)

def floatValide(number):
        return isinstance(number,float)

class ReadFile:

    def __init__(self, path):
        self.path = path

    def read_file(self):
        wb_obj = openpyxl.load_workbook(self.path)
        sheet_obj = wb_obj.active
        first_row = []

        for col in range(sheet_obj.max_column):
            first_row.append(sheet_obj.cell(1, col + 1).value)
        data = []

        for row in range(2, sheet_obj.max_row + 1):
            elm = {}
            for col in range(sheet_obj.max_column):
                elm[first_row[col]] = sheet_obj.cell(row, col + 1).value
            data.append(elm)
        return data

    def clean_data(self, data,clear_data,trash_data):
        cont=1
        for items in data:
            cont +=1
            if not intvalide(items.get('row_id')) or not floatValide(items.get('sales')) or not is_email(items.get('email')):
                items['fila_damage'] = cont
                trash_data.append(items)

            else:
                clear_data.append(items)

    def main_data(self, data):
        dic = {}
        for items in data:
            if dic.get(items.get('ship_mode')):
                dic.get(items.get('ship_mode')).append(items)
            else:
                dic[items.get('ship_mode')]=[items]
        return dic





def main():
    test = ReadFile('base.xlsx')
    clear_data=[]
    trash_data=[]
    test.clean_data(test.read_file(),clear_data,trash_data)
    main_data_for_ship=test.main_data(clear_data)
    n='0'
    menu = """
        1. Datos limpia ,
        2. Datos sucios,
        3. Filas a corregir
        4. Motrar los datos clasificados 
        0. para terminar
                   """
    while n == '0':
        print(menu)
        opcion= input('Ingresa tu opcion ')
        if opcion =='1':
            for x in clear_data:
                print(x)
        elif opcion =='2':
            for x in trash_data:
                print(x)
        elif opcion== '3':
            for x in trash_data:
                print(x['fila_damage'], end=' ')
        elif opcion =='4':
            pprint.pprint(main_data_for_ship,depth=3)
        elif opcion =='0':
            n='1'
        else:
            print('opcion incorrecta')
    #pprint.pprint(main_data_for_ship,depth=2)
    #pprint.pprint(trash_data)




if __name__ == '__main__':
    main()